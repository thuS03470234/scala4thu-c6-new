package apps

import cc.Fraction

/**
  * Created by mac006 on 2017/4/24.
  */
object FractionApp extends App{
  val frac=Fraction(1,2) plus Fraction(1,3)

  println(frac)

  val frac2=Fraction(1,2) equal Fraction(3,6)

  println(frac2)

  val frac3=Fraction(3,0)
  println(frac3)
}