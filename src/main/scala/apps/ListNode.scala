package apps
import cc.ListNode
/**
  * Created by mac006 on 2017/4/24.
  */
object ListNodeApp extends App{
  val listNode=ListNode(1,ListNode(2,ListNode(3,ListNode(4,ListNode(5,null)))))
  println(listNode)  //1->2->3->4->5

  val listNode1=ListNode(1,ListNode(2,ListNode(3,ListNode(4,ListNode(5,null)))))
  println(listNode1.size) //4
}
