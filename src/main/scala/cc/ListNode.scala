package cc



/**
  * Created by mark on 23/04/2017.
  */

case class ListNode(value:Int,next:ListNode) {
  def hasNext= next!=null

  def size :Int={
    if (hasNext) {
      next.size+1
    } else 1
  }

  def filter(f:Int=>Boolean):ListNode={
    if (ListNode!=f) {
      value+"->"+next.toString
    }else value
  } //會考喔！

  def prepend(elem:Int)= ???
  def delete(elem:Int):ListNode= ???

  override def toString: String= {
    if (hasNext) {
      value + "->" + next.toString
    } else value.toString
  }


}
